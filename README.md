## SB Admin v2.0 rewritten in AngularJS

[![Join the chat at https://gitter.im/start-angular/sb-admin-angular](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/start-angular/sb-admin-angular?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

This project is a port of the famous Free Admin Bootstrap Theme [SB Admin v2.0](http://startbootstrap.com/template-overviews/sb-admin-2/) to Angular Theme.

Find out more [Free Angular Themes at StartAngular.com](http://www.startangular.com/).

## Installation
####1. Clone this project or Download that ZIP file

```sh
$ git clone https://github.com/start-angular/sb-admin-angular.git
```

####2.  Make sure you have [bower](http://bower.io/), [grunt-cli](https://www.npmjs.com/package/grunt-cli) and  [npm](https://www.npmjs.org/) installed globally
 
 
```sh
$ sudo apt-get install npm
$ sudo npm install -g grunt-cli
$ sudo npm install -g bower
```
####3. On the command prompt run the following commands

```sh
$ cd `project-directory`
```
- bower install is ran from the postinstall
```sh
$ npm install 
```
- a shortcut for `grunt serve`
```sh
$ npm start
```
- a shortcut for `grunt serve:dist` to minify the files for deployment
```sh
$ npm run dist 
```


**Note:**
If you get this following error, 
```text
Error: EACCES, permission denied '.config/configstore/insight-bower.yml'
You don't have access to this file.
```
changing ownner .config

```sh
sudo chown -R [user name] ~/.config
```


## Roadmap

- Add sample AJAX calls and make the directives more modular

### Automation tools

- [Grunt](http://gruntjs.com/)










steps to integrate this app
1) go to C:\Windows\System32\drivers\etc
open hosts file and add the following line in the bottom
127.0.0.1       admin.mediplus.local


2) go to C:\wamp\bin\apache\apache2.4.9\conf 
open file httpd.conf and find line 
'#Include conf/extra/httpd-vhosts.conf'
and change it to 
'Include conf/extra/httpd-vhosts.conf'

3) go to C:\wamp\bin\apache\apache2.4.9\conf\extra
open file httpd-vhost.conf and add the follwoing lines in the end

<VirtualHost *:80>
    DocumentRoot "c:/wamp/www/admin_mediplus/app"
    ServerName admin.mediplus.local
   <Directory c:/wamp/www/admin_mediplus>
 Options FollowSymLinks
    AllowOverride None
    Order deny,allow
    Deny from all
		</Directory>
</VirtualHost>


for heena and ajinkya add the following lines instead of above lines
<VirtualHost *:81>
    DocumentRoot "c:/wamp/www/admin_mediplus/app"
    ServerName admin.mediplus.local
   <Directory c:/wamp/www/admin_mediplus>
 Options FollowSymLinks
    AllowOverride None
    Order deny,allow
    Deny from all
		</Directory>
</VirtualHost>

4) restart wamp and 
access
http://admin.mediplus.com
for heena and ajinkya access
http://admin.mediplus.com:81


